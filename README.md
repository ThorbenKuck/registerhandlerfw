# README #

### What is the RegisterHandler framework?
* The RegisterHandler framework is meant for Dependency-Injection, as well as module-control. It can create Registers and share instances of objects over multiple procedures, without the need of static atributes, or sharing instances of Objects.
* More comes later ... 

### What is this repository for? ###

* In this repository youll find a framework, called RegisterHandler.
* V.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone -> run gradlewrapper.

### Contribution guidelines ###

* Pull-Request are never (!) accepted manually! Please let the pull-request be reviewed multiple Times and never accept your own requests.
* If you want to contribute, please contact me!

### Who do I talk to? ###

* Repo owner (ThorbenKuck) or admin